Rails.application.routes.draw do
  root 'top#main'
  get 'top/main'
  resources :products
  resources :cartitems,only:[:create,:destroy]
  resources :carts
end
